![GoOpen](./assets/GoOpen.svg)

<div class="intro">
  <p>
    Welcome to GoOpen! The decentralized blog.
  </p>
  <domain />
</div>

<div>
  <h1>Donate</h1>
  <p>
    Bitcoin: 358c7U477iefGF67yg8XnhLqbuC1C5U7hg
  </p>
  <p>
    Ethereum: 0x2F28fE9D4A7fC37Eca286Cc2889e78b7c1a1EeCf
  </p>
  <p>
    Namecoin: N6AwVHLtSc8WyemNgqF4HTPuqMFfuFJWkS
  </p>
</div>
