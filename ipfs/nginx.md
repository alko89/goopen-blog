# Nginx

Nginx is a web server which can also be used as a reverse proxy.

```
server {
    listen 443 ssl;
    server_name goopen.eu www.goopen.eu goopen.bit www.goopen.bit;
    access_log      /var/log/nginx/ipfs.access.log;
    error_log       /var/log/nginx/ipfs.error.log;

    location / {
        proxy_pass http://127.0.0.1:8080/ipns/QmYybKeMr8oMePcKU2zXb3sYXtQVZfHUSoNGcffvgCWLTv/; # Note the trailing slash
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }

    ssl_certificate /etc/letsencrypt/live/goopen.eu/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/goopen.eu/privkey.pem; # managed by Certbot
}
```
