# Namecoin

Namecoin is an experimental open-source technology which improves decentralization, security, censorship resistance, privacy, and speed of certain components of the Internet infrastructure such as DNS and identities.

It can be used to registed dot-bit domains.

## Register domain

First check if the domain name is available.

```
./namecoin-cli name_show d/goopen
```

If the name is available you can now pre-order the name.

```
./namecoin-cli name_new d/goopen
```

You will get an output with two hashed numbers. Save them for later use.

Once you have ordered a name wait at least 12 confirmations before executing the next command.

```
./namecoin-cli name_firstupdate d/goopen "SHORT_HASH" "LONG_HASH" '{"ip":"1.2.3.4", "map": {"*": {"ip":"1.2.3.4"}}}'
```

## Update domain

To update the domain, use the `name_update` command.

```
./namecoin-cli name_update d/goopen '{"ip":"4.3.2.1", "map": {"*": {"ip":"4.3.2.1"}}}'
```

## Namecoin daemon

We need to configure Namecoin RPC config to allow RPC connections to Namecin daemon. Add following configuration to `namecoin.conf` file and restart the daemon:

```
rpcallowip=127.0.0.1/8
rpcport=8336
rpcauth=namecoinrpc:rpcpass
rpcuser=namecoinrpc
rpcpassword=rpcpass
```

Next we need to setup NCDNS a Namecoin to DNS bridge daemon.
