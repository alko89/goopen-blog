module.exports = {
  title: 'GoOpen',
  description: 'The decentralized blog.',
  head: [
    ['link', { rel: 'icon', href: '/favicon.png' }],
  ],
  themeConfig: {
    lastUpdated: 'Last Updated',
    logo: '/Go.svg',
    search: false,
    sidebar: [
      {
        title: 'IPFS',
        collapsable: false,
        children: [
          ['/ipfs/introduction', 'Introduction'],
          ['/ipfs/publish-site', 'Publish Website'],
          ['/ipfs/nginx', 'Nginx'],
        ],
      },
      {
        title: 'Namecoin',
        collapsable: false,
        children: [
          ['/namecoin/wallet', 'Wallet'],
          ['/namecoin/ncdns', 'NCDNS'],
        ],
      }
    ]
  },
} 
